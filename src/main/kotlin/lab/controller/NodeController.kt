package lab.controller

import lab.dto.NodeDto
import lab.service.NodeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/node")
class NodeController(@Autowired var service: NodeService) {

    @GetMapping("/all")
    @ResponseBody
    fun getNodes(): List<NodeDto> {
        return service.getNodes()
    }

    @PostMapping("/parse")
    @ResponseBody
    fun createNode(): ResponseEntity<NodeDto> {
        return ResponseEntity.ok().body(
            service.createNode()
        )
    }

    @GetMapping("/get/{id}")
    @ResponseBody
    fun getNode(@PathVariable("id") id: Long): ResponseEntity<NodeDto> {
        return ResponseEntity.ok().body(
            service.getNode(id)
        )
    }

    @PutMapping("/update")
    @ResponseBody
    fun updateNode(@RequestBody nodeDto: NodeDto): ResponseEntity<NodeDto> {
        return ResponseEntity.ok().body(
            service.updateNode(nodeDto)
        )
    }

    @DeleteMapping("/delete/{id}")
    fun deleteNode(@PathVariable("id") id: Long): ResponseEntity<Any> {
        service.deleteNode(id)
        return ResponseEntity.ok().build()
    }

    @PostMapping("/create")
    fun createNode(@RequestBody node: NodeDto): ResponseEntity<NodeDto> {
        return ResponseEntity.ok().body(
            service.createNode(node)
        )
    }

    @GetMapping("/search-in-radius")
    fun searchInRadius(@RequestParam("lon") longitude: Double,
                       @RequestParam("lat") latitude: Double,
                       @RequestParam("rad") radius: Double): ResponseEntity<List<NodeDto>> {
        return ResponseEntity.ok().body(
            service.findNodes(longitude, latitude, radius)
        )
    }
}