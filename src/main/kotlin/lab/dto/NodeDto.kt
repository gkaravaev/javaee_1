package lab.dto

import lab.repository.entity.NodeEntity
import java.math.BigInteger
import java.sql.Timestamp

data class NodeDto(
    val id: Long?,

    val tags: MutableList<TagDto>? = mutableListOf(),

    var lat: Double = 0.0,

    var lon: Double = 0.0,

    var user: String,

    var uid: BigInteger,

    var visible: Boolean = false,

    var version: BigInteger,

    var changeset: BigInteger,

    var timestamp: String
) {
    companion object {
        fun fromEntity(nodeEntity: NodeEntity): NodeDto {
            return NodeDto(
                id = nodeEntity.id,
                lat = nodeEntity.lat,
                lon = nodeEntity.lon,
                user = nodeEntity.user,
                uid = nodeEntity.uid,
                visible = nodeEntity.visible,
                version = nodeEntity.version,
                changeset = nodeEntity.changeset,
                timestamp = nodeEntity.timestamp.toString(),
                tags = nodeEntity.tags.map(TagDto.Companion::fromEntity).toMutableList()
            )
        }
    }

    fun toEntity(): NodeEntity {
        return NodeEntity(
            tags?.map(TagDto::toEntity)?.toMutableList() ?: mutableListOf(),
            lat,
            lon,
            user,
            uid,
            visible,
            version,
            changeset,
            Timestamp.valueOf(timestamp)
        )
    }
}