package lab.dto

import lab.repository.entity.TagEntity

data class TagDto(
    val id: Long?,

    val k: String,

    var v: String
) {
    companion object {
        fun fromEntity(tagEntity: TagEntity): TagDto {
            return TagDto(tagEntity.id, tagEntity.k, tagEntity.v)
        }
    }

    fun toEntity(): TagEntity {
        return TagEntity(k, v, null)
    }
}