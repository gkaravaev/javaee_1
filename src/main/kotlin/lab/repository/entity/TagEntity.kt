package lab.repository.entity

import lab.generated.xml.Tag
import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
@Table(name = "tag")
class TagEntity(

    val k: String,

    var v: String,

    @ManyToOne
    @JoinColumn(name = "node_id")
    var node: NodeEntity?

) : BaseEntity<Long>() {

    companion object {
        fun fromXmlClass(tag: Tag, node: NodeEntity): TagEntity {
            return TagEntity(
                k = tag.k, v = tag.v, node = node
            )
        }
    }

}