package lab.repository.entity

import lab.generated.xml.Node
import java.math.BigInteger
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "node")
class NodeEntity(
    @OneToMany(
        mappedBy = "node",
        fetch = FetchType.LAZY,
        orphanRemoval = true,
        cascade = [CascadeType.ALL]
    )
    var tags: MutableList<TagEntity> = mutableListOf(),

    var lat: Double = 0.0,

    var lon: Double = 0.0,

    @Column(name = "usr")
    var user: String,

    var uid: BigInteger,

    var visible: Boolean = false,

    var version: BigInteger,

    var changeset: BigInteger,

    var timestamp: Timestamp
) : BaseEntity<Long>() {
    companion object {
        fun fromXmlClass(node: Node): NodeEntity {
            return NodeEntity(
                changeset = node.changeset,
                lat = node.lat,
                lon = node.lon,
                user = node.user,
                uid = node.uid,
                visible = node.isVisible ?: true,
                version = node.version,
                timestamp = Timestamp.from(node.timestamp.toGregorianCalendar().toInstant())
            )
        }
    }

    fun withoutTags(): NodeEntity {
        tags = mutableListOf()
        return this
    }
}