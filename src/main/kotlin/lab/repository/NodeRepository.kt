package lab.repository

import lab.repository.entity.NodeEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param


interface NodeRepository : JpaRepository<NodeEntity, Long> {
    @Query(
        value = """
            SELECT * 
            FROM node
            WHERE (earth_distance(ll_to_earth(55, 83), 
                ll_to_earth(node.lat, node.lon)) < :rad + earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(55, 83)))
                AND (earth_distance(ll_to_earth(54, 82), ll_to_earth(node.lat, node.lon)) < :rad + earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(54, 82)))
                AND earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(node.lat, node.lon)) < :rad
            ORDER BY 
            earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(node.lat, node.lon)) 
            ASC
            """, nativeQuery = true
    )
    fun getNodesInRangeOrderByDistAsc(
        @Param("lat") latitude: Double,
        @Param("lon") longtitude: Double,
        @Param("rad") radius: Double
    ): List<NodeEntity>
}