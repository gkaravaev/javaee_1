package lab.repository

import lab.repository.entity.TagEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TagRepository: JpaRepository<TagEntity, Long> {
}