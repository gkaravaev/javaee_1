package lab.reader

import lab.generated.xml.Node
import lab.generated.xml.Tag
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import org.apache.logging.log4j.kotlin.logger
import java.io.FileInputStream
import java.io.InputStreamReader
import javax.xml.bind.JAXBContext
import javax.xml.stream.XMLEventReader
import javax.xml.stream.XMLInputFactory

class BZ2OSMReader(private var pathToFile: String) : AutoCloseable {

    private val events = arrayOf("node", "way", "relation")
    private val eventCounts = arrayOf(3895311328.0, 347989892.0, 5184942.0)
    private val log = logger("BZM2OSMLogger")
    private val nodeUnmarshaller = JAXBContext.newInstance(Node::class.java).createUnmarshaller()

    private lateinit var fileInputStream: FileInputStream
    private lateinit var bZip2StreamReader: InputStreamReader
    lateinit var reader: XMLEventReader

    private fun openStream() {
        fileInputStream = FileInputStream(pathToFile)
        bZip2StreamReader = InputStreamReader(BZip2CompressorInputStream(fileInputStream))
        reader = XMLInputFactory.newInstance().createXMLEventReader(bZip2StreamReader)
    }

    init {
        openStream()
    }

    public fun nodeFromStream(): Node {
        return nodeUnmarshaller.unmarshal(this.reader) as Node
    }

    public fun tagFromStream(): Tag {
        return nodeUnmarshaller.unmarshal(this.reader) as Tag
    }

    public fun getTag(): Tag {
        if (reader.peek() == null) {
            reopen()
        }
        val event = reader.peek()
        while (true) {
            if (event.isStartElement) {
                when (event.asStartElement().name.localPart) {
                    "tag" -> {
                        return tagFromStream()
                    }
                }
            }
            reader.nextEvent()
        }
    }

    public fun getNextNode(): Node {
        if (reader.peek() == null) {
            reopen()
        }
        while (true) {
            val event = reader.peek()
            if (event.isStartElement) {
                when (event.asStartElement().name.localPart) {
                    "node" -> {
                        val node = nodeFromStream()
                        loop@ while (true) {
                            val event1 = reader.peek()
                            if (event.isStartElement) {
                                when (event.asStartElement().name.localPart) {
                                    "node" -> {
                                        break@loop
                                    }
                                    "tag" -> {
                                        val tag = tagFromStream()
                                        node.tag.add(tag)
                                    }
                                }
                                if (reader.peek().equals(event1)) {
                                    reader.nextEvent()
                                }
                            }
                        }
                        return node
                    }
                }
            }
            reader.nextEvent()
        }
    }

    private fun reopen() {
        log.info("Reopening streams of OSM Reader")
        this.close()
        this.openStream()
    }

    override fun close() {
        reader.close()
        bZip2StreamReader.close()
        fileInputStream.close()
    }

}