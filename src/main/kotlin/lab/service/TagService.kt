package lab.service;

import lab.dto.TagDto
import lab.repository.TagRepository;
import lab.repository.entity.TagEntity
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Service
public class TagService(@Autowired val tagRepository: TagRepository) {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public fun createTag(tag: TagEntity): TagDto {
        return TagDto.fromEntity(tagRepository.save(tag))
    }
}
