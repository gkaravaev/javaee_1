package lab.service;

import lab.dto.NodeDto
import lab.reader.BZ2OSMReader
import lab.repository.NodeRepository;
import lab.repository.entity.NodeEntity
import lab.repository.entity.TagEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Service
public class NodeService(
    @Autowired
    val nodeRepository: NodeRepository,
    @Autowired
    val tagService: TagService,
    @Autowired
    @Lazy
    val nodeService: NodeService
) {
    val tarbz2file = "./src/main/resources/RU-NVS.osm.bz2"
    val reader = BZ2OSMReader(tarbz2file)

    @Transactional(readOnly = true)
    fun getNodes(): List<NodeDto> {
        return nodeRepository.findAll().map(NodeDto.Companion::fromEntity)
    }

    @Transactional(readOnly = true)
    fun createNode(): NodeDto {
        val node = reader.use { osmReader ->
            val fromXmlNode = osmReader.getNextNode()
            var node = NodeEntity.fromXmlClass(fromXmlNode)
            node = this.nodeService.saveNode(node)
            fromXmlNode.tag
                .map { tag -> TagEntity.fromXmlClass(tag, node) }
                .forEach { tag -> tagService.createTag(tag) }
            return@use nodeRepository.getOne(node.id)
        }
        return NodeDto.fromEntity(node)
    }

    fun createNode(dto: NodeDto): NodeDto {
        val node = saveFullNode(dto.toEntity())
        return NodeDto.fromEntity(node)
    }

    @Transactional
    fun deleteNode(id: Long) {
        nodeRepository.deleteById(id)
    }

    @Transactional
    fun getNode(id: Long): NodeDto {
        return NodeDto.fromEntity(nodeRepository.getOne(id))
    }

    @Transactional(readOnly = true)
    fun saveFullNode(nodeEntity: NodeEntity): NodeEntity {
        val tags = nodeEntity.tags
        val node = this.nodeService.saveNode(nodeEntity.withoutTags())
        tags.map { tag ->
            tag.node = node
            tag
        }.forEach { tag -> tagService.createTag(tag) }
        return nodeRepository.getOne(node.id)
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun saveNode(nodeEntity: NodeEntity): NodeEntity {
        return nodeRepository.save(nodeEntity)
    }

    @Transactional(readOnly = true)
    fun updateNode(nodeDto: NodeDto): NodeDto {
        val nodeEntity = nodeRepository.getOne(nodeDto.id)
        val newNodeEntity = nodeDto.toEntity()
        newNodeEntity.tags = nodeEntity.tags
        newNodeEntity.id = nodeEntity.id
        return NodeDto.fromEntity(saveFullNode(newNodeEntity))
    }

    @Transactional
    fun findNodes(longitude: Double, latitude: Double, radius: Double): List<NodeDto> {
        return nodeRepository
            .getNodesInRangeOrderByDistAsc(latitude, longitude, radius).map(NodeDto.Companion::fromEntity)
    }
}
